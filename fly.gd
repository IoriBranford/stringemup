extends KinematicBody2D

const SPEED = 400
var speed = 400
var velocity = Vector2()

func _ready():
	speed = SPEED
	var angle = randf() * 2 * PI
	velocity.x = cos(angle)
	velocity.y = sin(angle)
	velocity *= speed

func _physics_process(delta):
	move_and_slide(velocity)

	var slide_count = get_slide_count()
	var normal = Vector2()
	var hitweb = false
	for i in range(0, slide_count):
		var collision = get_slide_collision(i)
		var collider = collision.collider
		if collider and collider.is_in_group("Web"):
			collider.vibrate()
			hitweb = true
		normal += collision.normal

	if normal != Vector2.ZERO:
		normal = normal.normalized()
		velocity = -velocity.reflect(normal)
	if hitweb:
		velocity = velocity.normalized()
	if velocity.length_squared() < SPEED * SPEED:
		velocity += velocity.normalized()

func _on_Area2D_body_entered(body):
	if body.is_in_group("Player"):
		body.eat_fly(self)
