extends Label

signal all_flies_eaten

var num_eaten = 0
var num_flies = 0

func _ready():
	var flies = get_tree().get_nodes_in_group("Flies")
	num_flies = len(flies)
	text = str(num_eaten) + '/' + str(num_flies)

func _on_player_fly_eaten():
	num_eaten += 1
	text = str(num_eaten) + '/' + str(num_flies)
	if num_eaten == num_flies:
		emit_signal("all_flies_eaten")