extends KinematicBody2D

signal fly_eaten

const GRAVITY = 800
const WALK_SPEED = 400
const JUMP_FORCE = 400
var velocity = Vector2()
var up = Vector2.UP
var touchpos
var threadpath = NodePath()
var threadscript = preload("res://thread.gd")
var websound = preload("res://web.wav")
var sticksound = preload("res://webstick.wav")
var eatsounds = [ preload("res://eat1.wav"), preload("res://eat2.wav"), preload("res://eat3.wav") ]

func jump(inputdirection, touchpos):
	if up.dot(inputdirection) < -.5:
		var circleshape = $CollisionShape2D.shape as CircleShape2D
		if circleshape:
			up = -up
			position = touchpos + up*circleshape.radius

	var right = Vector2(-up.y, up.x)
	var jumpdirection = up + inputdirection.project(right)
	velocity = jumpdirection * JUMP_FORCE
	$AnimationPlayer.stop(true)

	update_thread(touchpos, true)
	create_thread(touchpos)

func create_thread(startpos):
	var threadbody = StaticBody2D.new()
	threadbody.set_script(threadscript)
	threadbody.init(startpos, position)

	var game = get_node("/root/game")
	game.add_child(threadbody)

	threadpath = get_path_to(threadbody)

	$websoundplayer.stream = websound
	$websoundplayer.play()

func walk(inputdirection, touchpos, delta):
	var right = Vector2(-up.y, up.x)
	var cross = up.cross(inputdirection)
	if cross < 0 :
		$AnimationPlayer.play("left")
	elif cross > 0:
		$AnimationPlayer.play("right")
	else:
		$AnimationPlayer.stop(true)
	velocity = right * cross * WALK_SPEED
	velocity += GRAVITY * delta * -up
	update_thread(touchpos, true)

func fall(delta):
	velocity.y += GRAVITY * delta
	update_thread(position)

func eat_fly(fly:Node2D):
	if not fly.is_queued_for_deletion():
		emit_signal("fly_eaten")
		fly.queue_free()
		$eatsoundplayer.stream = eatsounds[randi() % eatsounds.size()]
		$eatsoundplayer.play()

func update_thread(p, end=false):
	if threadpath.is_empty():
		return
	var thread = get_node(threadpath)
	if thread:
		thread.set_endpoint(p)
		if end:
			thread.collision_mask |= 2 # Player
			threadpath = NodePath()
			$websoundplayer.stream = sticksound
			$websoundplayer.play()

func _physics_process(delta):
	var inputdirection = Vector2(0, 0)
	inputdirection.x -= (Input.get_action_strength("player_left"))
	inputdirection.x += (Input.get_action_strength("player_right"))
	inputdirection.y -= (Input.get_action_strength("player_up"))
	inputdirection.y += (Input.get_action_strength("player_down"))
	inputdirection = inputdirection.normalized()

	var jumppressed = Input.is_action_just_pressed("player_jump")

	var targetrotation = 0
	var rotationspeed = 8
	var slide_count = get_slide_count()
	if slide_count > 0:
		var normal = Vector2()
		for i in range(0, slide_count):
			var collision = get_slide_collision(i)
			normal += collision.normal
			touchpos = collision.position
		up = normal.normalized()
		if touchpos:
			if jumppressed:
				jump(inputdirection, touchpos)
			else:
				walk(inputdirection, touchpos, delta)
				var right = Vector2(-up.y, up.x)
				targetrotation = right.angle()
				rotationspeed = 32
	else:
		fall(delta)
		if touchpos and threadpath.is_empty():
			create_thread(touchpos)

	rotation += (targetrotation - rotation) * delta * rotationspeed
	velocity = move_and_slide(velocity, up)
