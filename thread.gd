extends PhysicsBody2D

var vibrationtime = 0

func init(startpoint:Vector2, endpoint:Vector2):
	collision_layer = 8 # Web
	collision_mask = 0
	add_to_group("Web")

	var segment = SegmentShape2D.new()
	segment.a = startpoint
	segment.b = endpoint
	var shape = CollisionShape2D.new()
	shape.shape = segment
	shape.name = "CollisionShape2D"
	add_child(shape)

	var line = Line2D.new()
	line.width = 2
	line.default_color = Color(1, 1, 1)
	line.add_point(startpoint)
	line.add_point(endpoint)
	line.name = "Line2D"
	add_child(line)

func set_endpoint(endpoint:Vector2):
	$Line2D.set_point_position(1, endpoint)
	var segment = $CollisionShape2D.shape as SegmentShape2D
	if segment:
		segment.b = endpoint

func vibrate():
	if vibrationtime == 0:
		vibrationtime = .5

func _process(delta):
	vibrationtime = max(0, vibrationtime - delta)
	var cosine = cos(30*PI*vibrationtime)
	var line = $Line2D as Line2D
	line.width = 2 + (1 - cosine)
	var color = .5 + (cosine)/2
	line.default_color = Color(color, color, color, color)