extends Camera2D

var playercamera
var startposition
var startzoom
var targetposition
var targetzoom
var progress

func _on_numeaten_all_flies_eaten():
#	playercamera = get_node("/root/game/player/camera") as Camera2D
#	targetposition = position
#	targetzoom = zoom
#	startposition = playercamera.global_position
#	startzoom = playercamera.zoom
#	global_position = startposition
#	zoom = startzoom
#	playercamera.current = false
	current = true
	progress = 0

func _process(delta):
	if targetposition:
		progress = min(progress + delta, 1)
		global_position = lerp(startposition, targetposition, progress)
		zoom = lerp(startzoom, targetzoom, progress)