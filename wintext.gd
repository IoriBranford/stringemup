extends Label

func _on_numeaten_all_flies_eaten():
	visible = true

func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed:
			if event.scancode == KEY_ESCAPE:
				get_tree().quit()
			elif event.scancode == KEY_R:
				get_tree().reload_current_scene()