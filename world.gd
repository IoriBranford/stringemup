extends StaticBody2D

func _ready():
	var child_count = get_child_count()
	for i in range(0, child_count):
		var child = get_child(i)
		if child is CollisionPolygon2D:
			var poly = Polygon2D.new()
			poly.polygon = child.polygon
			poly.color = Color.tan
			child.add_child(poly)
